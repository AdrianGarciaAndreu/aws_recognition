import boto3

offense = ["Payaso", "Puta", "Pendeja" "Mierda", "Ignorante", "Majadero", "Tonto", "Despojo", "Estupido", "Estupida", "Matarte", "gentuza"]

def detect_text(photo, bucket):
	resp = False
	client=boto3.client('rekognition')
	response=client.detect_text(Image={'S3Object':{'Bucket':bucket,'Name':photo}})
                        
	textDetections=response['TextDetections']

	for text in textDetections:
		resp = isOffensive(text['DetectedText'])
		if resp == True:
			print(photo+" --> Photo with offensive content.")
			break

	return resp


def isOffensive(text):
	offensive = False
	for wd in offense:
		_wd = wd.lower()
		_text = text.lower()
		if _wd in _text:
			offensive = True
			break
	return offensive

def main():

	bucket='ai-offensive'
	photo=''

	s3=boto3.client('s3')
	s3Response = s3.list_objects(Bucket=bucket)['Contents']
	for o in s3Response:
		photo=o['Key']
		resp=detect_text(photo,bucket)


	print("All photos recognized")

if __name__ == "__main__":
    main()
