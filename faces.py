import boto3

client=boto3.client("rekognition","us-west-2")

bucket="ai-friend"
collectionId="faces_collection"
fileName='pons_sample.jpg'
threshold = 1
maxFaces=1
  
response=client.search_faces_by_image(CollectionId=collectionId,
                            Image={'S3Object':{'Bucket':bucket,'Name':fileName}},
                            FaceMatchThreshold=threshold,
                            MaxFaces=maxFaces)

faceMatches=response['FaceMatches']

for match in faceMatches:
    print (match['Face']['ExternalImageId']+" recognized!")
    print ('Similarity: ' + "{:.2f}".format(match['Similarity']) + "%")
    print
    
    


