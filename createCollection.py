import boto3

client=boto3.client("rekognition","us-west-2")
bucket = "ai-friend"

def add_faces_to_collection(bucket,_file,face,collection_id):
	response=client.index_faces(CollectionId=collection_id, Image={'S3Object':{'Bucket':bucket,'Name':_file}}, ExternalImageId=face, MaxFaces=1)

	print (_file + ' added')
	print ('Results for ' + _file)
	print('Faces indexed:')				
	for faceRecord in response['FaceRecords']:
		print('  Face ID: ' + faceRecord['Face']['FaceId'])
		print('  Location: {}'.format(faceRecord['Face']['BoundingBox']))




def main():
	collection_id='faces_collection'

	client.delete_collection(CollectionId=collection_id) #Se borra la coleccion
	client.create_collection(CollectionId=collection_id) #Se agrega una coleccion

	#Se le agregan elementos
	add_faces_to_collection(bucket, 'piqueras.png', 'piqueras', collection_id)
	add_faces_to_collection(bucket, 'piqueras2.jpg', 'piqueras', collection_id)
	add_faces_to_collection(bucket, 'pons.jpg', 'pons', collection_id)
	add_faces_to_collection(bucket, 'pons_1.jpg', 'pons', collection_id)
	add_faces_to_collection(bucket, 'pons_2.jpg', 'pons', collection_id)
	add_faces_to_collection(bucket, 'prats.jpg', 'prats', collection_id)
	add_faces_to_collection(bucket, 'sanchez.jpg', 'sanchez', collection_id)


if __name__ == "__main__":
    main()  
