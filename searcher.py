import boto3

def detect_labels(photo, bucket, tag):

	client=boto3.client('rekognition')
	response = client.detect_labels(Image={'S3Object':{'Bucket':bucket,'Name':photo}}, MaxLabels=40, MinConfidence=70)

	_tag = tag.lower()   
	for label in response['Labels']:
		_ftag = label['Name'].lower()
		if _ftag in _tag:
			print ("Finded at photo: "+photo)
			print ("Confidence: " + str(label['Confidence']))

	return len(response['Labels'])


def main():
	
	bucket='ai-search'
	photo=''
	s3=boto3.client('s3')
	s3Response = s3.list_objects(Bucket=bucket)['Contents']

	#Python version 2
	_tag = raw_input("What are you looking for in the image stock? (Enter a keyword): ")

	for o in s3Response:
		photo=o['Key']
		label_count=detect_labels(photo, bucket, _tag)

	print("All photos recognized")

if __name__ == "__main__":
    main()

